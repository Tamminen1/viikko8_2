/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko8_2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author Juha
 */
public class FXMLDocumentController implements Initializable {
    private String filename;
    private String text;
    @FXML
    private Label label;
    @FXML
    private Button button;
    @FXML
    private TextField inputField;
    @FXML
    private TextField inputField2;
    
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText(inputField.getText());
    }
    @FXML
    private void lataus(ActionEvent event) throws FileNotFoundException, IOException {
        System.out.println("You clicked me!");
        filename = inputField2.getText();
        BufferedReader br = new BufferedReader(new FileReader(filename));
        label.setText(br.readLine());
        br.close();
    }
    @FXML
    private void tallennus(ActionEvent event) throws FileNotFoundException, IOException {
        System.out.println("You clicked me!");
        text = inputField.getText();
        filename = inputField2.getText();
        BufferedWriter br = new BufferedWriter(new FileWriter(filename));
        br.write(text);
        br.close();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
